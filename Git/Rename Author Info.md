To change the name and/or email address recorded in existing commits, you must rewrite the entire history of your Git repository.

This action is destructive to your repository's history. If you're collaborating on a repository with others, it's considered bad practice to rewrite published history. You should only do this in an emergency.

Running this script rewrites history for all repository collaborators. After completing these steps, any person with forks or clones must fetch the rewritten history and rebase any local changes into the rewritten history.

Before running this script, you'll need:

1. The old email address that appears in the author/committer fields that you want to change
2. The correct name and email address that you would like such commits to be attributed to

# Open Terminal (for Mac users) or the command prompt (for Windows and Linux users).

# Create a fresh, bare clone of your repository:

git clone --bare https://github.com/user/repo.git
cd repo.git

# Copy and paste and save the following script, replacing the following variables based on the information you gathered:

The OLD_EMAIL, the CORRECT_NAME and the CORRECT_EMAIL

#!/bin/sh
 
git filter-branch --env-filter '

OLD_EMAIL="your-old-email@example.com"
CORRECT_NAME="Your Correct Name"
CORRECT_EMAIL="your-correct-email@example.com"

if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags


# Save the script, for example as "git-author-rewrite.sh" in the repo directory

# Execute the script

# Review the new Git history for errors.

# Push the corrected history to GitHub:

git push --force --tags origin

# Clean up the temporary clone:

cd ..
rm -rf repo.git

