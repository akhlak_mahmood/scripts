""" A script to change Windows desktop wallpaper """

import time
import shutil
import ctypes
import _winreg

BLACKBG = "black.jpg"

SPI_SETDESKWALLPAPER = 20


def set_background_to_black():
    global BLACKBG
    
    hKey = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, r"Control Panel\Desktop")
    PREVBG, type = _winreg.QueryValueEx (hKey, "Wallpaper")
    
    shutil.copy2(PREVBG, "new.jpg")
    
    ctypes.windll.user32.SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, BLACKBG, 0)
    print "Black background set!"
    
def restore_background():
    ctypes.windll.user32.SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, "new.jpg", 0)
    print "Background restored!"


if __name__ == "__main__":
    set_background_to_black()
    print "Waiting 5 seconds..."
    time.sleep(5)
    restore_background()
    
