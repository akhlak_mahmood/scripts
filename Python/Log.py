from __future__ import print_function, unicode_literals
from os.path import basename as bn
import inspect

__doc__ = """ A simple logging module.

    Just import the module and use the 3 functions.

        import Log

        Log.set_tag("LOGGER")
        Log.set_debug(True)

        Log.say(Log.cf(), "Hello, this is an info message.")
        Log.warn(Log.cf(), "Say() messages will not be logged if debug is not True.")
        Log.error(Log.cf(), "Error() and Warn() will always be logged.")

    String formating can be done easily.

        from Log import cf

        name = "Jack Sparrow"
        age = 23
        location = "Earth"

        Log.warn(cf(), "2 + 2 = {}, 3 * 2 = {}", 2+2, 3*2)
        Log.say(cf(), "Hello, I am {0}, {1}, speaking from {2}.", name, age, location)

"""

# When debug is True, say() messages will not be logged.
# And the warning messages will be logged as normal messages.
Debug = False

# If a file name is given, messages will be logged there.
LogFile = False

# The log tag
Tag = "Log"

def _flog(message, header=None):
    """ Helper function to log to a file. """
    if LogFile:
        if header is None:
            header = "[{}]".format(Tag.upper())
        try:
            fh = open(LogFile, 'a+')
            fh.write(header + " " + message + "\n")
            fh.close()
        except: pass


def say(frame, message, *formatargs):
    """ Log a message. Frame info must be passed using log.cf(). """

    header = "{}::MSG".format(Tag.upper())

    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    if Debug:
        print("{} {} [{}(), {}, {}]".format(header,
            message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message



def warn(frame, message, *formatargs):
    """ Log a warning message. Frame info must be passed using Log.cf(). """

    if Debug:
        header = "{}::WARNING".format(Tag.upper())
    else:
        header = "{}::MSG".format(Tag.upper())
    
    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    print("{} {} [{}(), {}, {}]".format(header,
        message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message


def error(frame, message, *formatargs):
    """ Log an error message. Frame info must be passed using Log.cf(). """

    header = "{}::ERROR".format(Tag.upper())

    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    print("{} {} [{}(), {}, {}]".format(header,
        message, frame.function, bn(frame.filename), frame.lineno))

    _flog("{} in {}() at {} line {}".format(message,
        frame.function, bn(frame.filename), frame.lineno), header)

    return message

def cf():
    """ Alias of current_frame(). """
    return inspect.getframeinfo(inspect.currentframe().f_back)


def set_debug(debug):
    """ Set the current debug. """
    global Debug
    Debug = debug

def set_tag(tag):
    """ Set the log tag. """
    global Tag
    Tag = tag

def ON():
    """ Turn debug on. """
    global Debug
    Debug = True
    
def turnon():
    ON()

def turn_on():
    ON()

def OFF():
    """ Turn debug off. """
    global Debug
    Debug = False

def turnoff():
    OFF()

def turn_off():
    OFF()

def xsay(frame, message, *formatargs):
    """ A helper function to skip say() easily. """
    if type(message) != str:
        message = "{}".format(message)
    else:
        message = message.format(*formatargs)

    return message

def Xsay(frame, message, *formatargs):
    """ A helper function to skip say() easily. """
    return xsay(frame, message, *formatargs)

if __name__ == "__main__":
    print(__doc__)
    input()
