from __future__ import print_function

import os
import traceback

try:
    import LogPrint
except:
    pass

try:
    raw_input
    PY = 2
except NameError:
    PY = 3
    raw_input = input

encryptExt = ".dll"
failed = 0


doc = """
    This script will recursively "encrypt" all the files in
    current directory and all sub directories.
    Encryption is done by appending an extension at the end of files.
    Also the names are rot13ed for obfuscation.
"""

def rot13(clear):
    CLEAR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    ROT13 = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'
    TABLE = {x: y for x, y in zip(CLEAR, ROT13)}

    r = []
    for c in clear:
        r.append(TABLE.get(c, c))
    return ''.join(r)


def Encrypt(fDir):
    global failed

    for f in os.listdir(fDir):
        try:
            oldpath = os.path.join(fDir, f)

            # ignore the non-ascii chars
            if PY == 2:
                g = f.decode('unicode_escape').encode('ascii','ignore')
            else:
                g = f.encode('ascii','ignore').decode('utf-8')

            newpath = os.path.join(fDir, rot13(g) + encryptExt)
            # newpath = os.path.join(fDir, f + encryptExt)

            if os.path.isdir(oldpath):
                # Dir, recurse
                Encrypt(oldpath)
            elif os.path.isfile(oldpath):
            	# ignore common extensions and already encrypted files
                if f.endswith((".py", ".lnk", encryptExt)):
                    print("\n# SKIP " + f)
                    continue

                os.rename(oldpath, newpath)
                print("\n# DONE " + f)

        except Exception as err:
            print("\n# FAILED " + f)
            failed += 1
            traceback.print_exc()


def Decrypt(fDir):
    global failed

    for f in os.listdir(fDir):
        try:
            oldpath = os.path.join(fDir, f)
            newpath = os.path.join(fDir, rot13(".".join(f.split(".")[:-1])))
            # newpath = os.path.join(fDir, ".".join(f.split(".")[:-1]))

            if os.path.isdir(oldpath):
                # Dir, recurse
                Decrypt(oldpath)
            elif os.path.isfile(oldpath):
            	# ignore if not already encrypted
                if not f.endswith(encryptExt):
                    print("\n# SKIP [not encrypted] " + f)
                    continue

                os.rename(oldpath, newpath)
                print("\n# DONE " + f)

        except Exception as err:
            print("\n# FAILED " + f)
            failed += 1
            traceback.print_exc()

if __name__ == "__main__":
    print(doc)

    if raw_input("Encrypt files? [y/n] ")== "y":
        Encrypt(os.getcwd())
    elif raw_input("Decrypt files? [y/n] ")== "y":
        Decrypt(os.getcwd())

    print("\nDone! {} failed!".format(failed))
    raw_input()
