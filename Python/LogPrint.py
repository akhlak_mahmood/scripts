""" This module logs all the print() messages to the file
named after the importing running script in the current 
working directory. Only importing is enough, nothing else is
needed. """

__author__ = "Akhlak Mahmood <mahmoodakhlak@gmail.com>"
__version__ = 1.2
__date__ = "April 24, 2015"

import sys
import os
import __main__ as main

class LogPrint(object):
    def __init__(self):
        self.__fout = None
        self.__orig = sys.stdout
        self.closed = False
        self.__hide = False
        self.__tab = True
        
        try:
            # if it's the interactive shell, __file__ will
            # not be defined
            self.name = os.path.basename(main.__file__) + ".txt"
        except NameError:
            # in that case, we wouldn't need to log to
            # file either
            self.name = None

        if self.name is not None:
            self.__fout = open(self.name, "w")

    def hide(self):
        self.__hide = True

    def show(self):
        self.__hide = False

    def original(self):
        return self.__orig

    def write(self, string):
        if self.__hide is False:
            self.__orig.write(string)

        # if file opened, write there
        if self.__fout is not None:
            # each write is called twice :@
            if self.__tab:
                string = "{}".format(string)

            self.__tab = not self.__tab

            self.__fout.write(string)
            self.__fout.flush()

    def flush(self):
        self.__orig.flush()
        self.__fout.flush()

    def close(self):
        self.__orig.close()
        if self.__fout is not None:
            self.__fout.close()
        self.closed = True

    def detach(self):
        return self.__orig.detach()

    @property
    def encoding(self):
        return self.__orig.encoding

    @property
    def errors(self):
        return self.__orig.errors

    def fileno(self):
        return self.__orig.fileno()

    def isatty(self):
        return self.__orig.isatty()

    @property
    def newlines(self):
        return self.__orig.newlines

    def shell(self):
        return self.__orig.shell()

    @property
    def tags(self):
        return self.__orig.tags

    def tell(self):
        return self.__orig.tell()

    def seek(self):
        return self.__orig.seek()

    def seekable(self):
        return self.__orig.seekable()

    def writable(self):
        return self.__orig.writable()

    def writelines(self, lines):
        if self.__fout is not None:
            self.__fout.writelines(lines)
        return self.__orig.writelines(lines)



# additional functions to control output
def hideStdOut():
    """ Disable printing to stdout.
    Useful when print() is done only for debugging. """
    sys.stdout.hide()

def showStdOut():
    """ Enable printing to stdout.
    Useful when print() is done only for debugging. """
    sys.stdout.show()


if __name__=="__main__":
    print("This module is not supposed to run directly,\n"
          "instead import this module to log the print() outputs "
          "to a log file.\nAs soon as you import this module, "
          "the process should be effective immediately.")
    input()
    
else:
    # Whenever this module is imported,
    # the mechanism should work automatically,
    # nothing else should need to be done.

    sys.stdout = LogPrint()
