import os
from hashlib import sha1
try:
    import LogPrint
except:
    pass

filesDir = "qPics"

if not os.path.exists(filesDir):
	filesDir = os.getcwd()

def githash(filepath):
    filesize_bytes = os.path.getsize(filepath)

    sha = sha1()
    sha.update(("blob %u\0" % filesize_bytes).encode('utf-8'))

    with open(filepath, 'rb') as f:
        while True:
            block = f.read(2**10) # Magic number: one-megabyte blocks.
            if not block: break
            sha.update(block)

    return sha.hexdigest()

for f in os.listdir(filesDir):
	if not os.path.isdir(f):
		# just a failsafe
		if f == ".sync": continue
		if f == "Renamer.py": continue

		print("\nProcessing " + f)
		try:
			sourcepath = os.path.join(os.getcwd(), filesDir, f)
			targetpath = os.path.join(os.getcwd(), filesDir, githash(sourcepath) + "." + f.split(".")[-1])

			if sourcepath != targetpath:
			    os.rename(sourcepath, targetpath)
			    print(sourcepath + " -> " + targetpath)
			    print("Done!")

			else:
			    print("Already hased!")

		except Exception as err:
			print("FAILED: " + f + " (" + str(err) + ")")
	else: print("Is a dir: " + f)

print("All Done!")

