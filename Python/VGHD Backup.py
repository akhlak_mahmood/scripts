""" A python script to run old demos in vghd """

import os
import shutil
import re
import datetime

vghd_path = "R:\\VGHD"

def getCurrID():
    for i in range(1000):
        currpath = os.path.join(vghd_path, "demos (" + str(i) + ").lst")
        if not os.path.exists(currpath):
            return str(i)

def getOldData(id):
    old_model = os.path.join(vghd_path, "models (" + str(id) + ").lst")
    if not os.path.exists(old_model):
        return None
    else:
        with open(old_model, 'rb') as content_file:
            data = content_file.read()
        temp = re.compile(r'(a\d+)_\d+.demo').findall(data)
        demos = []
        for t in temp:
            if not t in demos:
                demos.append(t)
        return demos


def createInfo(id):
    new_model = os.path.join(vghd_path, "models (" + str(id) + ").lst")
    with open(new_model, 'rb') as content_file:
        data = content_file.read()

    temp = re.compile(r'(a\d+)_\d+.demo').findall(data)

    demos = []
    olddemos = getOldData(int(id) - 1)
    for t in temp:
            if not t in demos:
                    demos.append(t)
    if olddemos is None: olddemos = demos
    info = os.path.join(vghd_path, "info (" + id + ").html")
    file = open(info, 'w+')
    file.write("<html><body>")
    file.write("<b>Total models: " + str(len(demos)) + "<br/>Backup on: " + datetime.datetime.now().strftime("%B %d, %A, %Y %I:%M %p") + "</b><hr/>")
    for demo in demos:
            fullimg = "data\\" + demo + "\\" + demo + "_full.png"
            infoxml = "data\\" + demo + "\\" + demo + ".xml"
            if demo in olddemos:
                file.write("<a href='" + infoxml + "'><img src='" + fullimg + "' height='300px'/></a>")
            else:
                file.write("<a href='" + infoxml + "'><img src='" + fullimg + "' height='300px' style='border:3px solid red; border-radius: 9px; padding 5px; margin:2px;'/></a>")

    file.write("</body></html>")
    file.close()


def createRestorer(id):
    restore = os.path.join(vghd_path, "restore (" + id + ").py")
    file = open(restore, 'w+')
    code = """
# Auto generated restoring script for """ + id + """th backup
# Generated on: """ + datetime.datetime.now().strftime("%B %d, %A, %Y %I:%M %p") + """

import os
import shutil

vghd_path = r'""" + vghd_path + """'

def getFile(filetype, idx):
    currpath = os.path.join(vghd_path, filetype + "s (" + str(idx) + ").lst")
    if not os.path.exists(currpath):
        raise currpath + " does NOT exists"
    else:
        return currpath
            
def restore(filetype, idx):
    file = getFile(filetype, idx)
    current = os.path.join(vghd_path, "data\\\\" + filetype +"s.lst")
    try:
        if os.path.exists(current):
            os.remove(current)
    except:
        raise "Failed to remove current " + filetype
    else:
        shutil.copyfile(file, current)
        print filetype + " - " + str(idx) + " copied!"

def run():
    idx = """ + id + """
    restore("demo", idx)
    restore("model", idx)
		
if __name__=="__main__":
    run()
    raw_input()
"""
    file.write(code)
    file.close()


def BackupCurrent():
    id = getCurrID()
    current_demo = os.path.join(vghd_path, "data\\demos.lst")
    new_demo = os.path.join(vghd_path, "demos (" + id + ").lst")
    shutil.copy(current_demo, new_demo)
    current_model = os.path.join(vghd_path, "data\\models.lst")
    new_model = os.path.join(vghd_path, "models (" + id + ").lst")
    shutil.copy(current_model, new_model)
    createInfo(id)
    createRestorer(id)
    print "Back up done!"


if __name__=="__main__":
    BackupCurrent()
    raw_input()
