# A simple script to list all files in a directory.
# Created for torrent uploading - a text file containing all the 
# Songs or videos.

import os

flist = []
ext = ''

dir = raw_input("Enter directory path:")
ext = raw_input("Enter extension, e.g. '.mp3':")

for nfile in os.listdir(dir):
    if nfile.endswith(ext):
        flist.append(nfile.split('.')[0])


f = open(os.path.join(dir, "filelist.txt"), "w+")
for item in flist:
    f.write(item + "\n\r")

f.close()
print "Done!", len(flist), "files listed!"
