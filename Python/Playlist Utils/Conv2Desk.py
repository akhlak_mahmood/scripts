# Convert Playlist to Desktop path
# Phone lists are supposed to be in m3u8
# All m3u8 will be converted to m3u inside a folder "Desktop"
# A new file Conv2Phone.py will be written there. You can use it to
# convert the lists back. The conversion will use the "PathConfiguration.Txt"
# file. Refer to that file and make necessary changes before executing this script.

import os
from os.path import exists as PX
from os.path import join as PJ

phoneListPath = os.getcwd()
deskListPath = PJ(os.getcwd(), "Desktop")

if not PX(deskListPath): os.mkdir(deskListPath)

phoneConverterString = """
import os
from os.path import exists as PX
from os.path import join as PJ

phoneListPath = r'""" + phoneListPath + """'
conv = []
pfp = open(PJ(phoneListPath, "PathConfiguration.Txt"))
for line in pfp.readlines():
    if "==" in line:
        phLn = line.split("==")[0].strip()
        dskLn = line.split("==")[1].strip()
        conv.append((phLn, dskLn))
pfp.close()

for f in os.listdir(os.getcwd()):
    if f.endswith(".m3u"):
        fp = open(f)
        lines = fp.readlines()
        fp.close()
        nf = open(PJ(phoneListPath, f.replace(".m3u", ".m3u8")), 'w+')
        for song in lines:
            if song.startswith("#"): continue
            if song.startswith("\\n"): continue
            if song is "\\n": continue
            for crit in conv:
                if crit[1] in song:
                    song = song.replace(crit[1], crit[0])
            nf.write(song)
        nf.close()
        print "Done ", f
"""
        
conv = []
pfp = open(PJ(phoneListPath, "PathConfiguration.Txt"))
for line in pfp.readlines():
    if "==" in line:
        phLn = line.split("==")[0].strip()
        dskLn = line.split("==")[1].strip()
        conv.append((phLn, dskLn))
pfp.close()

for f in os.listdir(os.getcwd()):
    if f.endswith(".m3u8"):
        fp = open(f)
        lines = fp.readlines()
        fp.close()
        nf = open(PJ(deskListPath, f.replace(".m3u8", ".m3u")), 'w+')
        for song in lines:
            for crit in conv:
                if crit[0] in song:
                    song = song.replace(crit[0], crit[1])
            nf.write(song)
        nf.close()
        print "Done ", f

nf = open(PJ(deskListPath, "Conv2Phone.py"), 'w+')
nf.write(phoneConverterString)
nf.close()

print "Conv2Phone written! All done!"
