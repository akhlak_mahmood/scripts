# Rewrite Playlist paths
# Phone lists are supposed to be in m3u8
# All songs in each playlist are checked to see
# if the song exists. If not, all the defined folders
# inside the PathConfiguration.Txt is searched to
# find the file. If found, the new path is written
# if not found, old missing path is left
# as was. However, the file names should be
# unique, else, for example, a song file
# 1.mp3 will be replaced in different folder
# with the same name.

# NOTE: Edit PathConfiguration.Txt before running

import os
from os.path import exists as PX
from os.path import join as PJ


phoneListPath = os.getcwd()

if PX(PJ(phoneListPath, "Playlists")) is False:
    print("Warning! {} invalid root."
    	     .format(phoneListPath))
    phoneListPath = None
    roots = [
        '/storage/sdcard0/Music/Playlists',
        '/storage/sdcard1/Music/Playlists'
    ]
    for r in roots:
        if PX(r):
            phoneListPath = r
            break
    if phoneListPath is None:
        print("Playlists path could not be determined!")
        raise
    else:
        print("Playlists path: {}".format(phoneListPath))
        

conv = []
pfp = open(PJ(phoneListPath, "PathConfiguration.Txt"))
for line in pfp.readlines():
    if "==" in line:
        phLn = line.split("==")[0].strip()
        if not PX(phLn):
            print("Warning! {} not exists!".format(phLn))
            raise
        else: conv.append(phLn)
pfp.close()

pathOk = 0
rewrite = 0
total = 0
totalPl = 0

for f in os.listdir(phoneListPath):
    if f.endswith(".m3u8"):
        print("Processing " + f)
        totalPl += 1
        fpath = PJ(phoneListPath, f)
        fp = open(fpath)
        lines = fp.readlines()
        fp.close()
        nf = open(fpath, 'w')
        for song in lines:
            song = song.strip()
            total += 1
            # print("Checking {}: {}".format(song, len(song)))
            if PX(song) is False:
                songfile = os.path.basename(song)
                print("Warning! {} missing!".format(song))
                for d in conv:
                    if PX(PJ(d, songfile)):
                        rewrite += 1
                        print("Found {} inside {}".format(songfile, d))
                        song = PJ(d, songfile)
                    else:
                        # print("Not {}".format(PJ(d, songfile)))
                        pass
            else:
                pathOk += 1
                print("OK: {}".format(song))
            nf.write(song + "\n")
        nf.close()
        print("Done " + f)


input("\nAll {} m3u8 processed! Total songs {}, path rewritten {}, path ok {}."
	    .format(totalPl, total, rewrite, pathOk))

