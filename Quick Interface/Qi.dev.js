// ==UserScript==
// @name                 Qi Library (Dev)
// @version              3.0
// @description          JavaScript Quick Interface Library
// @match                http://*
// ==/UserScript==

// The main function, all your codes should go here.
QiMain = function() {
try {

	qiL.info("Hello world!");

} catch(e) { alert(e.message); }
};

try { // QUICK INTERFACE (Dev) v3.0 //

//// Begin:: Qi Globals

/**
 * Current log level for console logging.
 * @type {Number}
 */
qi_Debug = 5;	// verbose = 5, info = 4, warn = 3, err = 2, fatal = 1, none = 0

/**
 * Current log level for alert message box logging.
 * @type {Number}
 */
qi_Alert = qi_Debug - 1;

qi_author_  = "Akhlak Mahmood";

qi_date_    = "April 25, 2015";

qi_version_ = 2.2;

//// End:: Qi Globals

//// Begin:: Error Handlers

/**
 * A class instance containing default error messages
 * @return {Object}		The class instance.
 */
qiErrMsg = new function()
{
    this.ArgErr = "Argument Error";
}();

/**
 * A simple Log class instance with console logging and alert messages.
 * @return {Object}		qiL log class instance.
 */
qiL = new function()
{
	/**
	 * Log a verbose message.
	 * @param  {String} message The message to log.
	 * @param  {String} func	The function the message was logged from.
	 * 							(Optional)
	 * @return {null}
	 */
	this.msg = function(message, func)
	{
		if(qi_Debug > 4)
		{
			if (typeof func !== 'undefined') func = " @" + func + "()";
			else func = "";
			console.log("Verbose: " + message +func);
			if(qi_Alert > 4) alert("Msg: " + message + func);
		}
	};
	
	/**
	 * Log a info message.
	 * @param  {String} message The message to log.
	 * @param  {String} func	The function the message was logged from.
	 * 							(Optional)
	 * @return {null}
	 */
	this.info = function(message, func)
	{
		if(qi_Debug > 3)
		{
			if (typeof func !== 'undefined') func = " @" + func + "()";
			else func = "";
			console.log("Info: " + message + func);
			if(qi_Alert > 3) alert("Info: " + message + func);
		}
	};
	
	/**
	 * Log a waring message. Which may cause problem in future.
	 * @param  {String} message The message to log.
	 * @param  {String} func	The function the message was logged from.
	 * 							(Optional)
	 * @return {null}
	 */
	this.warn = function(message, func)
	{
		if(qi_Debug > 2)
		{
			if (typeof func !== 'undefined') func = " @" + func + "()";
			else func = "";
			console.log("Warning: " + message + func);
			if(qi_Alert > 2) alert("Warn: " + message + func);
		}
	};
	
	/**
	 * Log an error message. Which is preventing further proceedings.
	 * @param  {String} message The message to log.
	 * @param  {String} func	The function the message was logged from.
	 * 							(Optional)
	 * @return {null}
	 */
	this.err = function(message, func)
	{
		if(qi_Debug > 1)
		{
			if (typeof func !== 'undefined') func = " @" + func + "()";
			else func = "";
			console.log("Error: " + message + func);
			if(qi_Alert > 1) alert("Err: " + message + func);
		}
	};
	
	/**
	 * Log a fatal error message. The execution can not proceed any further.
	 * Imediate termination is needed.
	 * This log is always done regardless of qi_Debug setting.
	 * @param  {String} message The message to log.
	 * @param  {String} func	The function the message was logged from.
	 * 							(Optional)
	 * @return {null}
	 */
	this.fatal = function(message, func)
	{
		if (typeof func !== 'undefined') func = " @" + func + "()";
		else func = "";
		console.log("Fatal: " + message + func);
		alert("Fatal: " + message + func);
	};
}();

/**
 * A string format function. Index must be provided inside the {} brackets.
 * @param  {String} format 	The string to be formatted followed by the agruments to format with.
 * @return {String}			The formatted string
 */
qiF = function(format)
{
	if(typeof format === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiF");
		return;
	}
	var args = Array.prototype.slice.call(arguments, 1);
	return format.replace(/{(\d+)}/g, function(match, number)
	{
		return typeof args[number] != 'undefined' ? args[number] : match;
	});
};

//// End:: Error Handlers

/**
 * Set a cookie with specified name and value.
 * @param  {String} name 	The cookie name.
 * @param  {String} value 	The value of the cookie.
 * @param  {Number} days 	No of days before the cookie expires. (optional)
 * @param  {String} path 	The path of the cookie. (optional)
 * @return {null}
 */
qiCreateCookie = function(name, value, days, path)
{
	var expires;
	
	if(typeof value === 'undefined' || typeof name === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiCreateCookie");
		return;
	}
  
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000)); 
		expires = "expires=" + date.toGMTString() + "; ";
	}
	else expires = "";
	
	if (typeof path === 'undefined') path = "/";
	
	document.cookie = name + "=" + value +"; " + expires + "path=" + path + ";";
	qiL.msg("Cookie set: " + name + "=" + value +"; " + expires + "path=" + path + ";", "qiCreateCookie");
};

/**
 * Read a cookie with specified name.
 * @param  {String} name 	The name of the cookie.
 * @return {String}			Null is returned if cookie does not exist.
 */
qiReadCookie = function(name)
{
	var nameEQ = name + "=";
	var ca     = document.cookie.split(';'); 
	var ret    = null;

	if(typeof name === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiReadCookie");
		return;
	}

	for(var i=0; i<ca.length; i++)
	{ 
		var c = ca[i]; 
		while (c.charAt(0)==' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) === 0) 
		ret = c.substring(nameEQ.length, c.length);
	}
	qiL.msg("Cookie read: " + name + "=" + ret, "qiReadCookie");
	return ret;
};

/**
 * Delete a cookie with the given name.
 * @param  {String} name 	The name of the cookie.
 * @return {null}
 */
qiEraseCookie = function(name)
{
	if(typeof name === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiEraseCookie");
		return;
	}
	qiCreateCookie(name, "", -1);
	qiL.msg("Cookie deleted: " + name, "qiReadCookie");
};

//// End:: Cookie Handlers

/**
 * Download a file to the disk.
 * @param  {String} fileURL The url of the file.
 * @param  {String} fileName The name of the file to use if can not be determined from the fileURL.
 * @return {null}
 */
qiSaveToDisk = function(fileURL, fileName)
{
	if(typeof fileURL === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiSaveToDisk");
		return;
	}
	qiL.msg(qiF("Saving {0} as {1} ...", fileURL, fileName), "qiSaveToDisk");
	var save      = document.createElement('a');
	save.href     = fileURL;
	save.target   = '_blank';
	save.download = fileName || 'unknown';

	var event = document.createEvent('Event');
	event.initEvent('click', true, true);
	save.dispatchEvent(event);
	(window.URL || window.webkitURL).revokeObjectURL(save.href);
	qiL.msg(qiF("Saving done: {0}.", fileURL), "qiSaveToDisk");
};


//// Begin:: Overlay Helpers

/**
 * Set an overlay on the top left corner of the page.
 * If already set but hidden show it.
 * @param  {String} innerHtml The innerHtml string of the overlay
 *                            If omitted, the default "qi ..." is shown.
 *                            The html can be set by @qiOverlayHtml() later.
 * @return {null}
 */
qiShowOverlay = function(innerHtml)
{
	var qi = document.getElementById("qiOverlay");
	if (!qi)
	{
		var e              = document.createElement('div');
		e.style.position   = 'fixed';
		e.style.top        = '0px';
		e.style.left       = '0px';
		e.style.color      = '#000';
		e.style.background = '#fff';
		e.style.opacity    = '1';
		e.id               = "qiOverlay";
		e.style.fontSize   = '32px';
		e.style.padding    = '1px';
		e.style.border     = '3px solid #000';
		e.style.zIndex     = '10000';

		if(typeof innerHtml === 'undefined') e.innerHTML = "qi ...";
		else e.innerHTML = innerHtml;

		document.body.appendChild(e);
	}
	else qi.style.visibility = 'visible';
};

/**
 * Hide the overlay
 * @return {null}
 */
qiHideOverlay = function()
{
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.style.visibility = 'hidden';
};

/**
 * Replace the contents of the overlay with a specified element.
 * @param  {Object} elem The HTML element to replace the contents with.
 * @return {null}      
 */
qiOverlayElement = function(elem)
{
	if(typeof elem === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiOverlayElement");
		return;
	}
	var wrap = document.createElement('div');
	wrap.appendChild(elem.cloneNode(true));

	var qi = document.getElementById('qiOverlay');
	if(qi) qi.innerHTML = wrap.innerHTML;
};

/**
 * Append an element in the overlay using appendChild.
 * @param  {Object} elem The HTML element to append.
 * @return {null}      
 */
qiOverlayAddElement = function(elem)
{
	if(typeof elem === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiOverlayAddElement");
		return;
	}
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.appendChild(elem);
};

/**
 * Set the HTML of the overlay replacing current contents.
 * @param  {String} htmlstr The HTML string to set to the overlay.
 * @return {null}         
 */
qiOverlayHtml = function(htmlstr)
{
	if(typeof htmlstr === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiOverlayHtml");
		return;
	}
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.innerHTML = htmlstr;	
};

/**
 * Append HTML at the overlay.
 * @param  {String} htmlstr The HTML string to append.
 * @return {null}         
 */
qiOverlayAddHtml = function(htmlstr)
{
	if(typeof htmlstr === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiOverlayAddHtml");
		return;
	}
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.innerHTML += htmlstr;
};

/**
 * Set an image on the overlay replacing the contents.
 * @param  {String} src The image src.
 * @return {null}     
 */
qiOverlayImage = function(src)
{
	if(typeof src === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiOverlayImage");
		return;
	}
	var e = document.createElement('img');
	e.src = src;
	qiOverlayElement(e);
};

//// End:: Overlay Helpers


/**
 * Get the innerHtml of an element
 * @param  {Object} elem The HTML element object
 * @return {String}      The innerHtml string
 */
qiInnerHtml = function(elem)
{
	if(typeof elem === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiInnerHtml");
		return;
	}
	var wrap = document.createElement('div');
	wrap.appendChild(elem.cloneNode(true));
	return wrap.innerHTML;
};

/**
 * Get an HTML element with specified tag name and attributes.
 * @param  {String} tagName The type of the HTML element.
 * @param  {Dictionary} context A dictionary containing the attributes.
 * @return {Object}         The created HTML element
 */
qiSimpleElement = function(tagName, context)
{
	if(typeof tagName === 'undefined' || typeof context === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiSimpleElement");
		return;
	}
	var e = document.createElement(tagName);
	for (var key in context)
	{
		if (context.hasOwnProperty(key)) e.setAttribute(key, context[key]);
	}
	return e;
};

/**
 * Get an HTML element with specified type and properties.
 * @param  {Dictionary} context The dictionary containing the properties including
 *                              the tag name, font, color, bg, id, classN, html. If
 *                              one or more property is omitted, the default value
 *                              will be used instead.
 * @return {Object}         The created HTML element
 */
qiGetElement = function(context)
{
    // the default values of expected parameters
	var defaults = {
		tag    : 'div',
		font   : '20px',
		color  : '#000',
		bg     : '#fff',
		id     : 'undefined',
		classN : 'undefined',
        html   : 'undefined',
	};

	if(typeof context === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiGetElement");
		return;
	}

	// extend a dictionary object to default values
	function extendDict()
	{
		for (var i = 1; i < arguments.length; i++)
		{
			for (var key in arguments[i])
			{
				if (arguments[i].hasOwnProperty(key))
				{
					arguments[0][key] = arguments[i][key];
				}
			}
		}
		return arguments[0];
	}
    context = extendDict(defaults, context);
	
	var e              = document.createElement(context.tag);
	e.style.top        = '0px';
	e.style.left       = '0px';
	e.style.color      = context.color;
	e.style.background = context.bg;
	e.style.opacity    = '1';
	e.style.fontSize   = context.font;
	e.style.padding    = '1px';
	e.style.border     = '2px solid #000';
	e.style.zIndex     = '9000';

	if(context.id != 'undefined') e.id=context.id;
	if(context.classN != 'undefined') e.className = context.classN;
	if(context.html === 'undefined') e.innerHTML="qi ...";
	else e.innerHTML = context.html;
	
	return e;
};

/**
 * Load an external javascript. The script tag will be inserted in the head.
 * @param  {[type]} script The script URL
 * @return {null}
 */
qiLoadScript = function(script)
{
	if(typeof script === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiLoadScript");
		return;
	}
	qiL.msg("Loading: " + script, "qiLoadScript");
	var my_script = document.createElement('script');
	my_script.setAttribute('src', script);
	document.head.appendChild(my_script);
	qiL.msg("Done loading: " + script, "qiLoadScript");
};

/**
 * If the current URL starts with the given prefix
 * @param  {String} urlstr The prefix string to check
 * @return {Bool}        true or false
 */
qiUrlStartswith = function(urlstr)
{
	if(typeof urlstr === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiUrlStartswith");
		return;
	}
	return (document.URL.lastIndexOf(urlstr, 0) === 0);
};

/**
 * If the current URL ends with the given suffix
 * @param  {String} urlstr The suffix string to check
 * @return {Bool}        true or false
 */
qiUrlEndswith = function(urlstr)
{
	if(typeof urlstr === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiUrlEndswith");
		return;
	}
	return document.URL.indexOf(urlstr, document.URL.length - urlstr.length) !== -1;
};

/**
 * If a string starts with the given prefix
 * @param  {String} str    The string to check
 * @param  {String} suffix The prefix string
 * @return {Bool}        true or false
 */
qiStartswith = function(str, prefix)
{
	if(typeof str === 'undefined' || typeof prefix === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiStartswith");
		return;
	}
	return (str.lastIndexOf(prefix, 0) === 0);
};

// if the str ends with the given suffix
/**
 * If a string ends with the given suffix
 * @param  {String} str    The string to check
 * @param  {String} suffix The suffix string
 * @return {Bool}        true or false
 */
qiEndswith = function(str, suffix)
{
	if(typeof str === 'undefined' || typeof suffix === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiEndswith");
		return;
	}
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

/**
 * If a list contains an item
 * @param  {Array} list the container list
 * @param  {Object} item the containing item
 * @return {Bool}      true if item exists
 */
qiContains = function(list, item)
{
	if(typeof list === 'undefined' || typeof item === 'undefined')
	{
		qiL.warn(qiErrMsg.ArgErr, "qiContains");
		return;
	}
	return (list.indexOf(item) > -1);
};

/**
 * Scroll down to the bottom of the page
 * @return {null}
 */
qiScrollDown = function()
{
	window.scrollTo(0, document.body.scrollHeight);
};

/**
 * Load jQuery if not already loaded
 * @return {null}
 */
qiLoadJquery = function()
{
	if (typeof jQuery === 'undefined')
		qiLoadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js");
};


} // QUICK INTERFACE END //
catch(err) { alert("Qi Error: " + err.message); }

// the starting function
(function() {
try {
	qiShowOverlay();
	qiOverlayHtml("<input type='button' onclick='QiMain();' value='Quick Interface' style='height:50px; width:350px'/>");
} catch(e) { alert(e.message); }
})();
