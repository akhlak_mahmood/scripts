// ==UserScript==
// @name         iFap Mobile Downloader
// @version      1.3
// @description  beta.imagefap.com bulk image downloader
// @author       Elma Spice
// @match        http://beta.imagefap.com/pictures/*
// @match        http://beta.imagefap.com/photo/*
// @grant        none
// ==/UserScript==

qiLinks = [];
qiThumbs = [];
qi_no = 0;

XDlSpec = function(){
try {
	qi_no = parseInt(prompt("Enter no: ", qi_no + 1), 10);
	if (qi_no != NaN && qi_no <= qiLinks.length)
	{
		document.getElementById('qpix').src = qiThumbs[qi_no];
		setTimeout(function(){
			if (confirm("Download this image?") === true)
				qiSaveToDisk(qiLinks[qi_no]);
      }, 3000); 
	}
	else alert("Invalid input!");
} catch(e) { alert(e.message); }
};


XGetLinks = function(){
try {
	var cancelledThrice = 0;
	var downloaded = 0;
	
	// load the swipe gallery links
	var thumbsHold = document.getElementsByClassName("placeholder");

    // get all the image links
    var divs = document.getElementsByClassName('imgWrap');
	
	for (var i = 0; i < divs.length; i++) {
		var link = divs[i].getElementsByTagName("img")[0];
		
		if(qiEndswith(link.getAttribute('src'), ".jpg") == true) {
			link = link.getAttribute('src');
		}
		else if(qiEndswith(link.getAttribute('lnk'), ".jpg") == true) {
			link = link.getAttribute('lnk');
		}
		else {
			qiL.err(qiF("Non JPG link {0}", link.src));
			return;
		}
		
		var thumb = thumbsHold[i].getElementsByTagName("img")[0];
		qiLinks.push(link);
		// the thumbnail
		qiThumbs.push(thumb.src);
	}
	
	qiL.info(qiF("Total {0} images found!", qiLinks.length));
	
	var start = parseInt(prompt("Enter start: ", 0), 10);
	var end = parseInt(prompt("Enter end: ", 1), 10);
    if (end > qiLinks.length) end = qiLinks.length;

	if (confirm("Download all images?") === true){
		for (var i=start; i < end; i++) {
			document.getElementById('qpix').src = qiThumbs[i];
			qiSaveToDisk(qiLinks[i]);
			downloaded++;
       		qiL.info(qiF("Downloading image {0}... ", downloaded));
       }
	}	
	qiOverlayHtml("<input type='button' onclick='XDlSpec();' value='Download Specific' style='height:50px; width:350px'/><br/><img id='qpix' src=''/>");
	document.getElementById('qiOverlay').style.opacity = 0.7;

} catch(e) { alert(e.message); }
};


QiMain = function(){
try {
	// to load the swipe gallery links, click the first image
	document.getElementsByClassName("placeholder")[0].click();
	
	qiOverlayHtml("<input type='button' onclick='' value='Please wait ...' style='height:50px; width:350px'/><br/><img id='qpix' src=''/>");

	// wait 3 secs before loading the links
	setTimeout(XGetLinks, 3 * 1000);
} catch(e) { alert(e.message); }
};

try { // QUICK INTERFACE v3.0 //
qi_Debug=5,qi_Alert=qi_Debug-1,qiErrMsg=new function(){this.ArgErr="Argument Error"},qiL=new function(){this.msg=function(e,i){qi_Debug>4&&(i=void 0!=i?"@"+i+"()":"",console.log("Verbose: "+e+i),qi_Alert>4&&alert("Msg: "+e+i))},this.info=function(e,i){qi_Debug>3&&(i=void 0!=i?"@"+i+"()":"",console.log("Info: "+e+i),qi_Alert>3&&alert("Inf: "+e+i))},this.warn=function(e,i){qi_Debug>2&&(i=void 0!=i?"@"+i+"()":"",console.log("Warning: "+e+i),qi_Alert>2&&alert("Warn: "+e+i))},this.err=function(e,i){qi_Debug>1&&(i=void 0!=i?"@"+i+"()":"",console.log("Error: "+e+i),qi_Alert>1&&alert("Err: "+e+i))},this.fatal=function(e,i){i=void 0!=i?"@"+i+"()":"",console.log("Fatal: "+e+i),alert("Fatal: "+e+i)}},qiF=function(e){var i=Array.prototype.slice.call(arguments,1);return e.replace(/{(\d+)}/g,function(e,n){return"undefined"!=typeof i[n]?i[n]:e})},qiCreateCookie=function(e,i,n,t){if(void 0==i||void 0==e)return void qiL.warn(qiErrMsg.ArgErr,"qiCreateCookie");var r;if(n){var o=new Date;o.setTime(o.getTime()+24*n*60*60*1e3),r="expires="+o.toGMTString()+"; "}else r="";void 0==t&&(t="/"),document.cookie=e+"="+i+"; "+r+"path="+t+";",qiL.msg("Cookie set: "+e+"="+i+"; "+r+"path="+t+";","qiCreateCookie")},qiReadCookie=function(e){if(void 0==e)return void qiL.warn(qiErrMsg.ArgErr,"qiReadCookie");for(var i=e+"=",n=document.cookie.split(";"),t=null,r=0;r<n.length;r++){for(var o=n[r];" "==o.charAt(0);)o=o.substring(1,o.length);0==o.indexOf(i)&&(t=o.substring(i.length,o.length))}return qiL.msg("Cookie read: "+e+"="+t,"qiReadCookie"),t},qiEraseCookie=function(e){return void 0==e?void qiL.warn(qiErrMsg.ArgErr,"qiEraseCookie"):(qiCreateCookie(e,"",-1),void qiL.msg("Cookie deleted: "+e,"qiReadCookie"))},qiSaveToDisk=function(e,i){qiL.msg(qiF("Saving {0} as {1} ...",e,i),"qiSaveToDisk");var n=document.createElement("a");n.href=e,n.target="_blank",n.download=i||"unknown";var t=document.createEvent("Event");t.initEvent("click",!0,!0),n.dispatchEvent(t),(window.URL||window.webkitURL).revokeObjectURL(n.href),qiL.msg(qiF("Saving done: {0}.",e),"qiSaveToDisk")},qiShowOverlay=function(e){var i=document.getElementById("qiOverlay");if(i)i.style.visibility="visible";else{var n=document.createElement("div");n.style.position="fixed",n.style.top="0px",n.style.left="0px",n.style.color="#000",n.style.background="#fff",n.style.opacity="1",n.id="qiOverlay",n.style.fontSize="32px",n.style.padding="1px",n.style.border="3px solid #000",n.style.zIndex="10000",n.innerHTML=void 0==e?"qi ...":e,document.body.appendChild(n)}},qiHideOverlay=function(){var e=document.getElementById("qiOverlay");e&&(e.style.visibility="hidden")},qiOverlayElement=function(e){var i=document.createElement("div");i.appendChild(e.cloneNode(!0));var n=document.getElementById("qiOverlay");n&&(n.innerHTML=i.innerHTML)},qiOverlayAddElement=function(e){var i=document.getElementById("qiOverlay");i&&i.appendChild(e)},qiOverlayHtml=function(e){var i=document.getElementById("qiOverlay");i&&(i.innerHTML=e)},qiOverlayAddHtml=function(e){var i=document.getElementById("qiOverlay");i&&(i.innerHTML+=e)},qiOverlayImage=function(e){var i=document.createElement("img");i.src=e,qiOverlayElement(i)},qiInnerHtml=function(e){var i=document.createElement("div");return i.appendChild(e.cloneNode(!0)),i.innerHTML},qiSimpleElement=function(e,i){var n=document.createElement(e);for(var t in i)i.hasOwnProperty(t)&&n.setAttribute(t,i[t]);return n},qiGetElement=function(e){function i(){for(var e=1;e<arguments.length;e++)for(var i in arguments[e])arguments[e].hasOwnProperty(i)&&(arguments[0][i]=arguments[e][i]);return arguments[0]}var n={tag:"div",font:"20px",color:"#000",bg:"#fff",id:"undefined",classN:"undefined",html:"undefined"};e=i(n,e);var t=document.createElement(e.tag);return t.style.top="0px",t.style.left="0px",t.style.color=e.color,t.style.background=e.bg,t.style.opacity="1","undefined"!=e.id&&(t.id=e.id),"undefined"!=e.classN&&(t.className=e.classN),t.style.fontSize=e.font,t.style.padding="1px",t.style.border="2px solid #000",t.style.zIndex="9000",t.innerHTML="undefined"==e.html?"qi ...":e.html,t},qiLoadScript=function(e){qiL.msg("Loading: "+e,"qiLoadScript");var i=document.createElement("script");i.setAttribute("src",e),document.head.appendChild(i),qiL.msg("Done loading: "+e,"qiLoadScript")},qiUrlStartswith=function(e){return 0===document.URL.lastIndexOf(e,0)},qiStartswith=function(e,i){return 0===e.lastIndexOf(i,0)},qiEndswith=function(e,i){return-1!==e.indexOf(i,e.length-i.length)},qiContains=function(e,i){return e.indexOf(i)>-1},qiScrollDown=function(){window.scrollTo(0,document.body.scrollHeight)},qiLoadJquery=function(){"undefined"==typeof jQuery&&qiLoadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js")};
} // QUICK INTERFACE END //
catch(err) { alert("Qi Error: " + err.message); }

// the starting function
(function(){
try{
	qiShowOverlay();
	qiOverlayHtml("<input type='button' onclick='QiMain();' value='Download Images' style='height:50px; width:350px'/>");
} catch(e) { alert(e.message); }
})();
