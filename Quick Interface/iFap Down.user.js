// ==UserScript==
// @name            iFap Down 
// @namespace       https://crystaldice.wordpress.com/
// @version         1.0
// @description     iFap auto download
// @author          Akhlak Mahmood
// @include        http://beta.imagefap.com/pictures/*
// @include        http://beta.imagefap.com/photo/*
// @run-at          document-end
// @grant           none
// ==/UserScript==

// track downloaded files
pinStack = [];

QiMain = function(){
try {
    // only if the hash is set
    if( window.location.hash.substr(1) != "" ) {
        qiL.msg("Inside a pictures page!", "QiMain");
        
        // get all the image links
        var divs = document.getElementsByClassName('imgWrap');
        
        // get image index
        var i = parseInt(window.location.hash.substr(1)) - 1;
        
        // get the image
        var link = divs[i].getElementsByTagName("img")[0];
		
        // get the link
		if(qiEndswith(link.getAttribute('src'), ".jpg") == true) {
			link = link.getAttribute('src');
		}
		else if(qiEndswith(link.getAttribute('lnk'), ".jpg") == true) {
			link = link.getAttribute('lnk');
		}
		else if(qiEndswith(link.getAttribute('src'), ".png") == true) {
			link = link.getAttribute('src');
		}
		else if(qiEndswith(link.getAttribute('lnk'), ".png") == true) {
			link = link.getAttribute('lnk');
		}
        
        // download the image when loaded
        if(pinStack.indexOf(link) < 0) {
            var image = new Image();

            image.onload = function () {
                qiSaveToDisk(link, "iFap Image");
                qiL.info("Photo downloaded!", "QiMain");
            }

            image.src = link;
            pinStack.push(link);
            if(pinStack.length > 50) pinStack.shift();
        }
        else qiL.msg("Already downloaded!", "QiMain");
    }
    else qiL.msg("Not a photo page!");

} catch(e) { alert(e.message); }
};

try { // QUICK INTERFACE (Dev) v3.0 //
qi_Debug=4,qi_Alert=3,qi_author_="Akhlak Mahmood",qi_date_="April 25, 2015",qi_version_=2.2,qiErrMsg=new function(){this.ArgErr="Argument Error"},qiL=new function(){this.msg=function(e,i){qi_Debug>4&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Verbose: "+e+i),qi_Alert>4&&alert("Msg: "+e+i))},this.info=function(e,i){qi_Debug>3&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Info: "+e+i),qi_Alert>3&&alert("Info: "+e+i))},this.warn=function(e,i){qi_Debug>2&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Warning: "+e+i),qi_Alert>2&&alert("Warn: "+e+i))},this.err=function(e,i){qi_Debug>1&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Error: "+e+i),qi_Alert>1&&alert("Err: "+e+i))},this.fatal=function(e,i){i="undefined"!=typeof i?" @"+i+"()":"",console.log("Fatal: "+e+i),alert("Fatal: "+e+i)}},qiF=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiF");var i=Array.prototype.slice.call(arguments,1);return e.replace(/{(\d+)}/g,function(e,n){return"undefined"!=typeof i[n]?i[n]:e})},qiCreateCookie=function(e,i,n,r){var t;if("undefined"==typeof i||"undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiCreateCookie");if(n){var o=new Date;o.setTime(o.getTime()+24*n*60*60*1e3),t="expires="+o.toGMTString()+"; "}else t="";"undefined"==typeof r&&(r="/"),document.cookie=e+"="+i+"; "+t+"path="+r+";",qiL.msg("Cookie set: "+e+"="+i+"; "+t+"path="+r+";","qiCreateCookie")},qiReadCookie=function(e){var i=e+"=",n=document.cookie.split(";"),r=null;if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiReadCookie");for(var t=0;t<n.length;t++){for(var o=n[t];" "==o.charAt(0);)o=o.substring(1,o.length);0===o.indexOf(i)&&(r=o.substring(i.length,o.length))}return qiL.msg("Cookie read: "+e+"="+r,"qiReadCookie"),r},qiEraseCookie=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiEraseCookie"):(qiCreateCookie(e,"",-1),void qiL.msg("Cookie deleted: "+e,"qiReadCookie"))},qiSaveToDisk=function(e,i){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiSaveToDisk");qiL.msg(qiF("Saving {0} as {1} ...",e,i),"qiSaveToDisk");var n=document.createElement("a");n.href=e,n.target="_blank",n.download=i||"unknown";var r=document.createEvent("Event");r.initEvent("click",!0,!0),n.dispatchEvent(r),(window.URL||window.webkitURL).revokeObjectURL(n.href),qiL.msg(qiF("Saving done: {0}.",e),"qiSaveToDisk")},qiShowOverlay=function(e){var i=document.getElementById("qiOverlay");if(i)i.style.visibility="visible";else{var n=document.createElement("div");n.style.position="fixed",n.style.top="0px",n.style.left="0px",n.style.color="#000",n.style.background="#fff",n.style.opacity="1",n.id="qiOverlay",n.style.fontSize="32px",n.style.padding="1px",n.style.border="3px solid #000",n.style.zIndex="10000",n.innerHTML="undefined"==typeof e?"qi ...":e,document.body.appendChild(n)}},qiHideOverlay=function(){var e=document.getElementById("qiOverlay");e&&(e.style.visibility="hidden")},qiOverlayElement=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayElement");var i=document.createElement("div");i.appendChild(e.cloneNode(!0));var n=document.getElementById("qiOverlay");n&&(n.innerHTML=i.innerHTML)},qiOverlayAddElement=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayAddElement");var i=document.getElementById("qiOverlay");i&&i.appendChild(e)},qiOverlayHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayHtml");var i=document.getElementById("qiOverlay");i&&(i.innerHTML=e)},qiOverlayAddHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayAddHtml");var i=document.getElementById("qiOverlay");i&&(i.innerHTML+=e)},qiOverlayImage=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayImage");var i=document.createElement("img");i.src=e,qiOverlayElement(i)},qiInnerHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiInnerHtml");var i=document.createElement("div");return i.appendChild(e.cloneNode(!0)),i.innerHTML},qiSimpleElement=function(e,i){if("undefined"==typeof e||"undefined"==typeof i)return void qiL.warn(qiErrMsg.ArgErr,"qiSimpleElement");var n=document.createElement(e);for(var r in i)i.hasOwnProperty(r)&&n.setAttribute(r,i[r]);return n},qiGetElement=function(e){function i(){for(var e=1;e<arguments.length;e++)for(var i in arguments[e])arguments[e].hasOwnProperty(i)&&(arguments[0][i]=arguments[e][i]);return arguments[0]}var n={tag:"div",font:"20px",color:"#000",bg:"#fff",id:"undefined",classN:"undefined",html:"undefined"};if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiGetElement");e=i(n,e);var r=document.createElement(e.tag);return r.style.top="0px",r.style.left="0px",r.style.color=e.color,r.style.background=e.bg,r.style.opacity="1",r.style.fontSize=e.font,r.style.padding="1px",r.style.border="2px solid #000",r.style.zIndex="9000","undefined"!=e.id&&(r.id=e.id),"undefined"!=e.classN&&(r.className=e.classN),r.innerHTML="undefined"===e.html?"qi ...":e.html,r},qiLoadScript=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiLoadScript");qiL.msg("Loading: "+e,"qiLoadScript");var i=document.createElement("script");i.setAttribute("src",e),document.head.appendChild(i),qiL.msg("Done loading: "+e,"qiLoadScript")},qiUrlStartswith=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiUrlStartswith"):0===document.URL.lastIndexOf(e,0)},qiUrlEndswith=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiUrlEndswith"):-1!==document.URL.indexOf(e,document.URL.length-e.length)},qiStartswith=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiStartswith"):0===e.lastIndexOf(i,0)},qiEndswith=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiEndswith"):-1!==e.indexOf(i,e.length-i.length)},qiContains=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiContains"):e.indexOf(i)>-1},qiScrollDown=function(){window.scrollTo(0,document.body.scrollHeight)},qiLoadJquery=function(){"undefined"==typeof jQuery&&qiLoadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js")};
} // QUICK INTERFACE END //
catch(err) { alert("Qi Error: " + err.message); }

// the starting function
(function(){
try{
    qiShowOverlay();
    qiOverlayHtml("<input type='button' onclick='QiMain();' value='Download' style='height:25px; width:300px; opacity:0.7;'/>");
    setInterval(function(){ QiMain(); }, 1000);
} catch(e) { alert(e.message); }
})();
