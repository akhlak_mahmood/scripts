// ==UserScript==
// @name            TumblrDown
// @namespace       https://crystaldice.wordpress.com/
// @version         1.0
// @description     Auto download image by clicking on tumblr
// @author          Elma Spice
// @include         *.tumblr.com*
// @run-at          document-end
// @grant           none
// ==/UserScript==

// to maintain a history of last 50 downloaded files
dloadedStack = [];

// start processing the link to download the image
processLink = function(link)
{

    qiL.msg(qiF("Processing {0}", link), "processLink");

    if(dloadedStack.indexOf(link) < 0) {
        var image = new Image();

        image.onload = function ()
        {
            qiL.info(link, "processQueue");
            qiSaveToDisk(link, "da Image");
            qiL.info("Download Started!", "processQueue");
        }

        image.src = link;

        dloadedStack.push(link);
        if(dloadedStack.length > 50) dloadedStack.shift();
    }
    else {
        qiL.msg("Already downloaded!", "processQueue");
    }
}


QiMain = function(){
    try {
        qiL.msg("Registering listener ...", "QiMain");
        document.addEventListener('click', function(event) {
            
            event = event || window.event;

            // get the element clicked
            var target = event.target || event.srcElement;

            // disable default click action
            event.preventDefault();
            
            // if not an image element, we are not gonna download
            var isImg = (target.nodeName.toLowerCase() === 'img');
            if(isImg) {
                processLink(target.src);
            }
            else if(target.getElementsByTagName('img').length > 0){
                target = target.getElementsByTagName('img')[0];
                processLink(target.src);
            }   
            else qiL.msg("Not clicked on an image!", "QiMain");
            
            console.log(target);

        }, false);
    } catch(e) { alert(e.message); }
};

try { // QUICK INTERFACE v3.0 //
    qi_Debug=4,qi_Alert=3,qi_author_="Elma Spice",qi_date_="April 25, 2015",qi_version_=2.2,qiErrMsg=new function(){this.ArgErr="Argument Error"},qiL=new function(){this.msg=function(e,i){qi_Debug>4&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Verbose: "+e+i),qi_Alert>4&&alert("Msg: "+e+i))},this.info=function(e,i){qi_Debug>3&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Info: "+e+i),qi_Alert>3&&alert("Info: "+e+i))},this.warn=function(e,i){qi_Debug>2&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Warning: "+e+i),qi_Alert>2&&alert("Warn: "+e+i))},this.err=function(e,i){qi_Debug>1&&(i="undefined"!=typeof i?" @"+i+"()":"",console.log("Error: "+e+i),qi_Alert>1&&alert("Err: "+e+i))},this.fatal=function(e,i){i="undefined"!=typeof i?" @"+i+"()":"",console.log("Fatal: "+e+i),alert("Fatal: "+e+i)}},qiF=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiF");var i=Array.prototype.slice.call(arguments,1);return e.replace(/{(\d+)}/g,function(e,n){return"undefined"!=typeof i[n]?i[n]:e})},qiCreateCookie=function(e,i,n,r){var t;if("undefined"==typeof i||"undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiCreateCookie");if(n){var o=new Date;o.setTime(o.getTime()+24*n*60*60*1e3),t="expires="+o.toGMTString()+"; "}else t="";"undefined"==typeof r&&(r="/"),document.cookie=e+"="+i+"; "+t+"path="+r+";",qiL.msg("Cookie set: "+e+"="+i+"; "+t+"path="+r+";","qiCreateCookie")},qiReadCookie=function(e){var i=e+"=",n=document.cookie.split(";"),r=null;if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiReadCookie");for(var t=0;t<n.length;t++){for(var o=n[t];" "==o.charAt(0);)o=o.substring(1,o.length);0===o.indexOf(i)&&(r=o.substring(i.length,o.length))}return qiL.msg("Cookie read: "+e+"="+r,"qiReadCookie"),r},qiEraseCookie=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiEraseCookie"):(qiCreateCookie(e,"",-1),void qiL.msg("Cookie deleted: "+e,"qiReadCookie"))},qiSaveToDisk=function(e,i){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiSaveToDisk");qiL.msg(qiF("Saving {0} as {1} ...",e,i),"qiSaveToDisk");var n=document.createElement("a");n.href=e,n.target="_blank",n.download=i||"unknown";var r=document.createEvent("Event");r.initEvent("click",!0,!0),n.dispatchEvent(r),(window.URL||window.webkitURL).revokeObjectURL(n.href),qiL.msg(qiF("Saving done: {0}.",e),"qiSaveToDisk")},qiShowOverlay=function(e){var i=document.getElementById("qiOverlay");if(i)i.style.visibility="visible";else{var n=document.createElement("div");n.style.position="fixed",n.style.top="0px",n.style.left="0px",n.style.color="#000",n.style.background="#fff",n.style.opacity="0.7",n.id="qiOverlay",n.style.fontSize="32px",n.style.padding="1px",n.style.border="3px solid #000",n.style.zIndex="10000",n.innerHTML="undefined"==typeof e?"qi ...":e,document.body.appendChild(n)}},qiHideOverlay=function(){var e=document.getElementById("qiOverlay");e&&(e.style.visibility="hidden")},qiOverlayElement=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayElement");var i=document.createElement("div");i.appendChild(e.cloneNode(!0));var n=document.getElementById("qiOverlay");n&&(n.innerHTML=i.innerHTML)},qiOverlayAddElement=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayAddElement");var i=document.getElementById("qiOverlay");i&&i.appendChild(e)},qiOverlayHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayHtml");var i=document.getElementById("qiOverlay");i&&(i.innerHTML=e)},qiOverlayAddHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayAddHtml");var i=document.getElementById("qiOverlay");i&&(i.innerHTML+=e)},qiOverlayImage=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiOverlayImage");var i=document.createElement("img");i.src=e,qiOverlayElement(i)},qiInnerHtml=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiInnerHtml");var i=document.createElement("div");return i.appendChild(e.cloneNode(!0)),i.innerHTML},qiSimpleElement=function(e,i){if("undefined"==typeof e||"undefined"==typeof i)return void qiL.warn(qiErrMsg.ArgErr,"qiSimpleElement");var n=document.createElement(e);for(var r in i)i.hasOwnProperty(r)&&n.setAttribute(r,i[r]);return n},qiGetElement=function(e){function i(){for(var e=1;e<arguments.length;e++)for(var i in arguments[e])arguments[e].hasOwnProperty(i)&&(arguments[0][i]=arguments[e][i]);return arguments[0]}var n={tag:"div",font:"20px",color:"#000",bg:"#fff",id:"undefined",classN:"undefined",html:"undefined"};if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiGetElement");e=i(n,e);var r=document.createElement(e.tag);return r.style.top="0px",r.style.left="0px",r.style.color=e.color,r.style.background=e.bg,r.style.opacity="1",r.style.fontSize=e.font,r.style.padding="1px",r.style.border="2px solid #000",r.style.zIndex="9000","undefined"!=e.id&&(r.id=e.id),"undefined"!=e.classN&&(r.className=e.classN),r.innerHTML="undefined"===e.html?"qi ...":e.html,r},qiLoadScript=function(e){if("undefined"==typeof e)return void qiL.warn(qiErrMsg.ArgErr,"qiLoadScript");qiL.msg("Loading: "+e,"qiLoadScript");var i=document.createElement("script");i.setAttribute("src",e),document.head.appendChild(i),qiL.msg("Done loading: "+e,"qiLoadScript")},qiUrlStartswith=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiUrlStartswith"):0===document.URL.lastIndexOf(e,0)},qiUrlEndswith=function(e){return"undefined"==typeof e?void qiL.warn(qiErrMsg.ArgErr,"qiUrlEndswith"):-1!==document.URL.indexOf(e,document.URL.length-e.length)},qiStartswith=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiStartswith"):0===e.lastIndexOf(i,0)},qiEndswith=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiEndswith"):-1!==e.indexOf(i,e.length-i.length)},qiContains=function(e,i){return"undefined"==typeof e||"undefined"==typeof i?void qiL.warn(qiErrMsg.ArgErr,"qiContains"):e.indexOf(i)>-1},qiScrollDown=function(){window.scrollTo(0,document.body.scrollHeight)},qiLoadJquery=function(){"undefined"==typeof jQuery&&qiLoadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js")};
} // QUICK INTERFACE END //
catch(err) { alert("Qi Error: " + err.message); }

// the starting function
(function(){
    try{
        qiShowOverlay();
        QiMain();
        qiOverlayHtml("Tumblr Down Running..!");
    } catch(e) { alert(e.message); }
})();
