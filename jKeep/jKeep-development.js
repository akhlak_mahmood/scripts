/* ************************

/// Use the following to access this library
/// Also allow extension's local file access

// @require      file:///H:/jKeep/src/jKeep-development.js


/// Define a Main function inside the jKeep namespace.

jKeep.Main = function()
{
    jKeep.showOverlay();
    jKeep.overlaySetHtml("<input type='button' onclick='jKeep.parse();' value='CLICK ME!' style='height:50px; cursor:pointer; font-weight:bold; width:350px'/>");
    
    jKeep.parse = function()
    {
        jKeep.msg("Hello! You clicked the button!");
    };
};

/// Now call this Main() function by running jKeep
jKeep.Run();

**************************/

(function(window){

    function define_jKeep() {
        var jKeep = {};

        // version is the last update date        
        jKeep.version = 160405;
        
        jKeep.Main = function() {
            jKeep.fatal("You must define your own jKeep.Main() function");
        };

        var executed = false;

        jKeep.Run = function() {
            if (executed === true)
                jKeep.warn("jKeep.Main() already executed");

            else {

                try {
                    jKeep.Main();
                }
                catch(e) { jKeep.fatal(e.message); }

                executed = true;
            }
        };

        // *************** BEG: LOGGING **************** //

        // define log levels
        jKeep.levels = {
            "debug": 6,
            "verbose": 5,
            "info": 4,
            "warn": 3,
            "err": 2,
            "fatal": 1,
            "none": 0
        };

        // set default console and alert logging levels
        var loglvl = jKeep.levels.verbose;
        var alertlvl = jKeep.levels.warn;

        // set new console logging level
        jKeep.setLog = function(lvl) {
            loglvl = lvl;
            alertlvl = loglvl - 1;
        };

        // alert logging level is automattically set one step down
        // to the console logging level
        // it can be modified using this method
        jKeep.setAlert = function(lvl) {
            alertlvl = lvl;
        };


        jKeep.printStack = function() {
            console.log("jKeep-Stack: " + new Error().stack);
        };


        jKeep.msg = function(message) {
            if(loglvl >= jKeep.levels.verbose)
            {
                console.log("jKeep: " + message);
                if(loglvl >= jKeep.levels.debug)
                    console.log("Call stack: " + new Error().stack);
                if(alertlvl >= jKeep.levels.verbose)
                    alert("jKeep: " + message);
            }
        };

        jKeep.fmsg = function(format) {
            var args = Array.prototype.slice.call(arguments, 1);
            var msg = format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
            jKeep.msg(msg);
        };


        jKeep.info = function(message)
        {
            if(loglvl >= jKeep.levels.info)
            {
                console.log("jK-INFO: " + message);
                if(loglvl >= jKeep.levels.debug)
                    console.log("Call stack: " + new Error().stack);
                if(alertlvl >= jKeep.levels.info)
                    alert("jK-INFO: " + message);
            }
        };

        jKeep.finfo = function(format) {
            var args = Array.prototype.slice.call(arguments, 1);
            var msg = format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
            jKeep.info(msg);
        };


        jKeep.warn = function(message)
        {
            if(loglvl >= jKeep.levels.warn)
            {
                console.log("jK-WARNING: " + message);
                if(loglvl >= jKeep.levels.debug)
                    console.log("Call stack: " + new Error().stack);
                if(alertlvl >= jKeep.levels.warn)
                    alert("jK-WARNING: " + message);
            }
        };

        jKeep.fwarn = function(format) {
            var args = Array.prototype.slice.call(arguments, 1);
            var msg = format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
            jKeep.warn(msg);
        };

        jKeep.err = function(message)
        {
            if(loglvl >= jKeep.levels.err)
            {
                console.log("jK-ERROR: " + message);
                if(loglvl >= jKeep.levels.debug)
                    console.log("Call stack: " + new Error().stack);
                if(alertlvl >= jKeep.levels.err)
                    alert("jK-ERROR: " + message);
            }
        };

        jKeep.ferr = function(format) {
            var args = Array.prototype.slice.call(arguments, 1);
            var msg = format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
            jKeep.err(msg);
        };

        jKeep.fatal = function(message)
        {
            console.log("jK-FATAL: " + message);
            if(loglvl >= jKeep.levels.debug)
                console.log("Call stack: " + new Error().stack);
            alert("jK-FATAL: " + message);
        };

        // *************** END: LOGGING **************** //
    

        // ******** BEG: String & URL Helpers ********* //
    

        // A string format function.
        // Indices must be provided inside the {} brackets.
        jKeep.fmt = function(format)
        {
            if(typeof format === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }

            var args = Array.prototype.slice.call(arguments, 1);
            return format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
        };

        jKeep.contains = function(list, item)
        {
            if(typeof list === 'undefined' || typeof item === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }
            return (list.indexOf(item) > -1);
        };


        // if the str starts with the prefix
        jKeep.startsWith = function(str, prefix)
        {
            if(typeof str === 'undefined' || typeof prefix === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }
            return (str.lastIndexOf(prefix, 0) === 0);
        };

        // if the str ends with the given suffix
        jKeep.endsWith = function(str, suffix)
        {
            if(typeof str === 'undefined' || typeof suffix === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }

            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        };

        // If the current URL starts with the given prefix
        jKeep.urlStartsWith = function(prefix)
        {
            if(typeof prefix === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            return (document.URL.lastIndexOf(prefix, 0) === 0);
        };

        // If the current URL ends with the given suffix
        jKeep.urlEndsWith = function(suffix)
        {
            if(typeof suffix === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            return document.URL.indexOf(suffix, document.URL.length - suffix.length) !== -1;
        };

        // ******** END: String & URL Helpers ********* //

        // ********* BEG: Automation Methods ********** //

        jKeep.saveToDisk = function(fileURL, fileName)
        {
            if(typeof fileURL === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }

            jKeep.finfo("Downloading {0}", fileURL);
            var save      = document.createElement('a');
            save.href     = fileURL;
            save.target   = '_blank';
            save.download = fileName || 'unknown';

            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
            jKeep.fmsg("Downloaded {0}", fileURL);
        };

        jKeep.scrollDown = function()
        {
            window.scrollTo(0, document.body.scrollHeight);
        };

        // Load an external javascript. The script tag will be inserted in the head.
        jKeep.loadScript = function(script)
        {
            if(typeof script === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            jKeep.msg("Loading script: " + script);
            var my_script = document.createElement('script');
            my_script.setAttribute('src', script);
            document.head.appendChild(my_script);
            jKeep.msg("Script loaded: " + script);
        };

        jKeep.loadJquery = function()
        {
            if (typeof jQuery === 'undefined')
                jKeep.loadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js");
        };

        // ********* END: Automation Methods ********** //

        // ********** BEG: Overlay Helpers *********** //

        // Set an overlay on the top left corner of the page.
        // If already set but hidden, show it.
        jKeep.showOverlay = function(innerHtml)
        {
            var qi = document.getElementById("jKeepOverlay");
            if (!qi)
            {
                var e              = document.createElement('div');
                e.style.position   = 'fixed';
                e.style.top        = '0px';
                e.style.left       = '0px';
                e.style.color      = '#000';
                e.style.background = '#fff';
                e.style.opacity    = '1';
                e.id               = "jKeepOverlay";
                e.style.fontSize   = '32px';
                e.style.padding    = '1px';
                e.style.border     = '3px solid #000';
                e.style.zIndex     = '10000';

                if(typeof innerHtml === 'undefined') e.innerHTML = "jKeep";
                else e.innerHTML = innerHtml;

                document.body.appendChild(e);
            }
            else qi.style.visibility = 'visible';
        };

        // Hide the overlay
        jKeep.hideOverlay = function()
        {
            var qi = document.getElementById('jKeepOverlay');
            if(qi) qi.style.visibility = 'hidden';
        };

        // Replace the contents of the overlay.
        // elem is the HTML element to replace the contents with.
        jKeep.overlaySetElement = function(elem)
        {
            if(typeof elem === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }

            var wrap = document.createElement('div');
            wrap.appendChild(elem.cloneNode(true));

            var qi = document.getElementById('jKeepOverlay');
            if(qi) qi.innerHTML = wrap.innerHTML;
        };

        // Append an element in the overlay using appendChild.
        jKeep.overlayAppendElement = function(elem)
        {
            if(typeof elem === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var qi = document.getElementById('jKeepOverlay');
            if(qi) qi.appendChild(elem);
        };

        // Set the HTML of the overlay replacing current contents.
        jKeep.overlaySetHtml = function(htmlstr)
        {
            if(typeof htmlstr === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var qi = document.getElementById('jKeepOverlay');
            if(qi) qi.innerHTML = htmlstr;  
        };

        // Append HTML at the overlay.
        jKeep.overlayAppendHtml = function(htmlstr)
        {
            if(typeof htmlstr === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var qi = document.getElementById('jKeepOverlay');
            if(qi) qi.innerHTML += htmlstr;
        };

        // Set an image on the overlay replacing the contents.
        jKeep.overlaySetImage = function(src)
        {
            if(typeof src === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var e = document.createElement('img');
            e.src = src;
            jKeep.overlaySetElement(e);
        };

        // ********** END: Overlay Helpers *********** //

        // ************ BEG: HTML Helpers ************* //

        // Get the string of an element
        jKeep.innerHtml = function(elem)
        {
            if(typeof elem === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var wrap = document.createElement('div');
            wrap.appendChild(elem.cloneNode(true));
            return wrap.innerHTML;
        };


        // Get an HTML element with specified tag name and attributes.
        // tagName - The type of the HTML element.
        // context - A dictionary containing the attributes.
        jKeep.simpleElement = function(tagName, context)
        {
            if(typeof tagName === 'undefined' || typeof context === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }
            var e = document.createElement(tagName);
            for (var key in context)
            {
                if (context.hasOwnProperty(key)) e.setAttribute(key, context[key]);
            }
            return e;
        };

        // Get an HTML element with specified type and properties.
        // context - A dictionary containing the properties including
        //          the tag name, font, color, bg, id, classN, html. If
        //          one or more property is omitted, the default value
        //          will be used instead.
        jKeep.makeElement = function(context)
        {
            // the default values of expected parameters
            var defaults = {
                tag    : 'div',
                font   : '20px',
                color  : '#000',
                bg     : '#fff',
                id     : 'undefined',
                classN : 'undefined',
                html   : 'undefined',
                opacity: '1'
            };

            if(typeof context === 'undefined')
            {
                jKeep.warn("Argument Error");
                return;
            }

            // extend a dictionary object to default values
            function extendDict()
            {
                for (var i = 1; i < arguments.length; i++)
                {
                    for (var key in arguments[i])
                    {
                        if (arguments[i].hasOwnProperty(key))
                        {
                            arguments[0][key] = arguments[i][key];
                        }
                    }
                }
                return arguments[0];
            }
            context = extendDict(defaults, context);
            
            var e              = document.createElement(context.tag);
            e.style.top        = '0px';
            e.style.left       = '0px';
            e.style.color      = context.color;
            e.style.background = context.bg;
            e.style.opacity    = context.opacity;
            e.style.fontSize   = context.font;
            e.style.padding    = '1px';
            e.style.border     = '2px solid #000';
            e.style.zIndex     = '9000';

            if(context.id != 'undefined') e.id=context.id;
            if(context.classN != 'undefined') e.className = context.classN;
            if(context.html === 'undefined') e.innerHTML="jKeep";
            else e.innerHTML = context.html;
            
            return e;
        };
        // ************ END: HTML Helpers ************* //

        // ************ BEG: Cookie Helpers ************* //

        // Set a cookie with specified name and value.
        jKeep.setCookie = function(name, value, days, path)
        {
            var expires;
            
            if(typeof value === 'undefined' || typeof name === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }
          
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days*24*60*60*1000)); 
                expires = "expires=" + date.toGMTString() + "; ";
            }
            else expires = "";
            
            if (typeof path === 'undefined') path = "/";
            
            document.cookie = name + "=" + value +"; " + expires + "path=" + path + ";";
            jKeep.msg("Cookie set: " + name + "=" + value +"; " + expires + "path=" + path + ";");
        };


        // Read a cookie with specified name.
        jKeep.getCookie = function(name)
        {
            var nameEQ = name + "=";
            var ca     = document.cookie.split(';'); 
            var ret    = null;

            if(typeof name === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }

            for(var i=0; i<ca.length; i++)
            { 
                var c = ca[i]; 
                while (c.charAt(0)==' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0) 
                ret = c.substring(nameEQ.length, c.length);
            }
            jKeep.msg("Cookie read: " + name + "=" + ret);
            return ret;
        };

        // Delete a cookie with the given name.
        jKeep.delCookie = function(name)
        {
            if(typeof name === 'undefined') {
                jKeep.warn("Argument Error");
                return;
            }
            jKeep.setCookie(name, "", -1);
            jKeep.msg("Cookie deleted: " + name);
        };

        // ************ END: Cookie Helpers ************* //

        // ************ BEG: Download Helpers ************* //

        var dloadQueue = [];
        var dloadedList = [];
        var dloadedListSize = 100;
        var processTime = 3; // secs

        jKeep.initQueue = function(listSize, time)
        {
            dloadedListSize = listSize;
            processTime = time;
        };

        jKeep.addToQueue = function(link)
        {
            if(dloadQueue.indexOf(link) < 0)
                dloadQueue.push(link);
        };

        jKeep.queueLength = function()
        {
            return dloadQueue.length;
        };

        jKeep.queueContains = function(link)
        {
            return (dloadQueue.indexOf(link) >= 0);
        };

        jKeep.isDloaded = function(link)
        {
            return (dloadedList.indexOf(link) >= 0);
        };

        // start processing the queue to download files
        jKeep.downloadQueue = function()
        {
            // normally the queue shouldn't be empty
            if(dloadQueue.length === 0) {
                jKeep.warn("Empty Queue Found");
                return;
            }

            var link = dloadQueue.pop();
            jKeep.overlaySetHtml(jKeep.fmt("<b>~{0}</b>", dloadQueue.length));

            jKeep.fmsg("Processing {0}", link);

            if(dloadedList.indexOf(link) < 0) {
                var image = new Image();

                image.onload = function ()
                {
                    jKeep.saveToDisk(link, "Image");

                    if(dloadQueue.length > 0) setTimeout(jKeep.downloadQueue, processTime * 1000);
                    else jKeep.msg("All images processed");
                };

                image.src = link;

                dloadedList.push(link);
                if(dloadedList.length > dloadedListSize) dloadedList.shift();
            }
            else {
                jKeep.msg("Already downloaded");
                if(dloadQueue.length > 0) jKeep.downloadQueue();
                else jKeep.msg("All images processed");
            }
        };

        // process a single link to download
        jKeep.downloadLink = function(link)
        {
            jKeep.fmsg("Processing {0}", link);

            if(dloadedList.indexOf(link) < 0) {
                var image = new Image();

                image.onload = function ()
                {
                    jKeep.saveToDisk(link, "Image");
                };

                image.src = link;

                dloadedList.push(link);
                if(dloadedList.length > dloadedListSize) dloadedList.shift();
            }
            else {
                jKeep.msg("Already downloaded");
            }
        };

        // ************ END: Download Helpers ************* //

        return jKeep;
    }
    
    // define globally if it doesn't already exist
    if(typeof(jKeep) === 'undefined') {
        window.jKeep = define_jKeep();
        jKeep.msg("jKeep initiated");
    }
    else {
        console.log("jKeep already defined");
    }
})(window);
