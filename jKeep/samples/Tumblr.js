// ==UserScript==
// @name            jK Tumblr Down
// @namespace       https://crystaldice.wordpress.com/
// @version         1.0
// @description     Auto download image by clicking on tumblr
// @author          Elma Spice
// @include         *.tumblr.com*
// @run-at          document-end
// @grant           none
// @require         file:///H:/jKeep/src/jKeep-development.js
// ==/UserScript==

jKeep.Main = function()
{
    jKeep.showOverlay();

    jKeep.msg("Registering click listener");

    document.addEventListener('click', function(event) {

        event = event || window.event;

        // get the element clicked
        var target = event.target || event.srcElement;

        // disable default click action
        // event.preventDefault();

        // if not an image element, we are not gonna download
        var isImg = (target.nodeName.toLowerCase() === 'img');
        if(isImg) {
            jKeep.downloadLink(target.src);
        }
        else if(target.getElementsByTagName('img').length > 0){
            target = target.getElementsByTagName('img')[0];
            jKeep.downloadLink(target.src);
        }   
        else jKeep.msg("Not clicked an image element");

        // console.log(target);

    }, false);
    
    jKeep.msg("Done registering click listener");
};

jKeep.Run();
