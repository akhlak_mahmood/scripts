// ==UserScript==
// @name            jK Love Honey Down
// @namespace       https://crystaldice.wordpress.com/
// @version         1.0
// @description     Auto download image when opened in deviant art
// @author          Elma Spice
// @match           *www.lovehoney.co.uk/product.cfm?p=*
// @run-at          document-end
// @grant           none
// @require         file:///H:/jKeep/src/jKeep-development.js
// ==/UserScript==


jKeep.Main = function()
{
    // init
    jKeep.showOverlay();
    jKeep.overlaySetHtml("<input type='button' onclick='jKeep.parse();' value='Download Images' style='height:50px; cursor:pointer; font-weight:bold; width:350px'/>");
    
    // parse and download
    jKeep.parse = function()
    {
        if(document.getElementsByClassName("pictures collapse").length > 0) {

            jKeep.msg("Inside a product page");
            
            // get all the image elements
            var divs = document.getElementsByClassName('cloudzoom-gallery');

            // add the images sources to the download queue
            for(var i = 0; i < divs.length; ++i) {
                var link = divs[i];
                if(link.href.length > 0) {
                    jKeep.addToQueue(link.href);
                }
            }

            jKeep.finfo("Found {0} images", jKeep.queueLength());
            jKeep.overlaySetHtml(jKeep.fmt("<b>~{0}</b>", jKeep.queueLength()));

            jKeep.downloadQueue();
        }
        else jKeep.msg("Not a product page");
    };
};


jKeep.Run();
