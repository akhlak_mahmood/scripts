// ==UserScript==
// @name            jK iFap Down
// @namespace       https://crystaldice.wordpress.com/
// @version         1.0
// @description     Auto download image by clicking on iFap
// @author          Elma Spice
// @include         http://www.imagefap.com/photo/*
// @run-at          document-end
// @grant           none
// @require         file:///H:/jKeep/src/jKeep-development.js
// ==/UserScript==

jKeep.Main = function()
{
    jKeep.showOverlay();

    jKeep.parse = function() {
        var div = document.getElementsByClassName('image-wrapper')[0];
        if(div.getElementsByTagName('img').length > 0){
            var target = div.getElementsByTagName('img')[0];
            jKeep.downloadLink(target.src);
        }
    };

    jKeep.msg("Registering click listener");
    
    document.getElementById("slideshow").addEventListener('click', function(event) {
        setTimeout(jKeep.parse, 500);
    }, false);
    
    jKeep.msg("Done registering click listener");
};

jKeep.Run();
