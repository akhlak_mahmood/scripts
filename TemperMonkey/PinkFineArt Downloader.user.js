// ==UserScript==
// @name        	Pink Fine Art Downloader
// @namespace 		http://akhlakmahmood.net/
// @version      	0.1
// @description 	Pink Fine Art Downloader
// @match       	http://pinkfineart.com/*/*
// @run-at        document-end
// @grant         none
// ==/UserScript==

/////////////////////
// QI STARTS HERE //
///////////////////

//// Begin::Globals
qi_Debug = 5;	// verbose = 5, info = 4, warn = 3, err = 2, fatal = 1, none = 0
qi_Alert = qi_Debug - 1; // verbose = 5, info = 4, warn = 3, err = 2, fatal = 1, none = 0
//// End::Globals

//// Begin:: Error Handlers

// List of default error messages
qiErrMsg = new function()
{
    this.ArgErr = "Argument Error";
};

// Log a message
qiL = new function()
{
  this.msg = function(message, func){
		if(qi_Debug > 4) {
			if (func != undefined) func = "@" + func + "()";
			else func = "";
			console.log("Verbose: " + message +func);
			if(qi_Alert > 4) alert("Msg: " + message + func);
		}
	};
  this.info = function(message, func){
		if(qi_Debug > 3) {
			if (func != undefined) func = "@" + func + "()";
			else func = "";
			console.log("Info: " + message + func);
			if(qi_Alert > 3) alert("Inf: " + message + func);
		}
	};
  this.warn = function(message, func){
		if(qi_Debug > 2) {
			if (func != undefined) func = "@" + func + "()";
			else func = "";
			console.log("Warning: " + message + func);
			if(qi_Alert > 2) alert("Warn: " + message + func);
		}
	};
  this.err = function(message, func){
		if(qi_Debug > 1) {
			if (func != undefined) func = "@" + func + "()";
			else func = "";
			console.log("Error: " + message + func);
			if(qi_Alert > 1) alert("Err: " + message + func);
		}
	};
  this.fatal = function(message, func){
		if (func != undefined) func = "@" + func + "()";
		else func = "";
		console.log("Fatal: " + message + func);
		alert("Fatal: " + message + func);
	};
};

// A string format functions
// qi_format('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
// returns "ASP is dead, but ASP.NET is alive! ASP {2}"
qiF = function(format)
{
	var args = Array.prototype.slice.call(arguments, 1);
	return format.replace(/{(\d+)}/g, function(match, number) { 
		return typeof args[number] != 'undefined' ? args[number] : match;
	});
};

//// End:: Error Handlers

//// Begin:: Cookie Handlers
// Set a cookie. Days and path are optional
qiCreateCookie = function(name, value, days, path)
{
  if(value == undefined || name == undefined){
    qiL.warn(qiErrMsg.ArgErr, "qiCreateCookie");
		return;
  }
  
	var expires;
	
  if (days) { 
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000)); 
    expires = "expires=" + date.toGMTString() + "; ";
  }
  else expires = "";
	if (path == undefined){
		path = "/";
	}
  document.cookie = name + "=" + value +"; " + expires + "path=" + path + ";";
	qiL.msg("Cookie set: " + name + "=" + value +"; " + expires + "path=" + path + ";", "qiCreateCookie");
};

// Read a cookie, if not found, null is returned
qiReadCookie = function(name)
{
	if(name == undefined){
    qiL.warn(qiErrMsg.ArgErr, "qiReadCookie");
		return;
	}
  var nameEQ = name + "=";
  var ca = document.cookie.split(';'); 
	var ret = null;

  for(var i=0; i<ca.length; i++) { 
    var c = ca[i]; 
    while (c.charAt(0)==' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) 
    ret = c.substring(nameEQ.length, c.length);
  }
	qiL.msg("Cookie read: " + name + "=" + ret, "qiReadCookie");
  return ret; 
};

// Delete a cookie with the given name
qiEraseCookie = function(name)
{
	if(name == undefined){
    qiL.warn(qiErrMsg.ArgErr, "qiEraseCookie");
		return;
	}
	qiCreateCookie(name, "", -1);
	qiL.msg("Cookie deleted: " + name, "qiReadCookie");
};

//// End:: Cookie Handlers

// save a file to the disk
qiSaveToDisk = function(fileURL, fileName)
{
	qiL.msg(qiF("Saving {0} as {1} ...", fileURL, fileName), "qiSaveToDisk");
	var save = document.createElement('a');
	save.href = fileURL;
	save.target = '_blank';
	save.download = fileName || 'unknown';

	var event = document.createEvent('Event');
	event.initEvent('click', true, true);
	save.dispatchEvent(event);
	(window.URL || window.webkitURL).revokeObjectURL(save.href);
	qiL.msg(qiF("Saving done: {0}.", fileURL), "qiSaveToDisk");
};

//// Begin:: Overlay Helpers

// Set a overlay on the top left corner of the page
// If already set and hidden it will show the overlay
qiShowOverlay = function(tag)
{
	var qi = document.getElementById('qiOverlay');
	if (!qi){
		var e=document.createElement('div');
		e.style.position='fixed';
		e.style.top='0px';
		e.style.left='0px';
		e.style.color='#000';
		e.style.background='#fff';
		e.style.opacity='1';
		e.id='qiOverlay';
		e.style.fontSize='32px';
		e.style.padding='1px';
		e.style.border='3px solid #000';
		e.style.zIndex='10000';
		if(tag == undefined) e.innerHTML="qi ...";
		else e.innerHTML = tag;
		document.body.appendChild(e);
	}
	else qi.style.visibility = 'visible';
};

// Hide the overlay
qiHideOverlay = function()
{
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.style.visibility = 'hidden';
};

// Replace the contents of the qiOverlay with the given element
qiOverlayElement = function(elem)
{
	var wrap = document.createElement('div');
	wrap.appendChild(elem.cloneNode(true));

	var qi = document.getElementById('qiOverlay');
	if(qi) qi.innerHTML = wrap.innerHTML;
};

// Set overlay innerHTML
qiOverlayHtml = function(htmlstr)
{
	var qi = document.getElementById('qiOverlay');
	if(qi) qi.innerHTML = htmlstr;	
};

// Replace the contents of the qiOverlay with the given image
qiOverlayImage = function(src)
{
	var e = document.createElement('img');
	e.src = src;
	qiOverlayElement(e);
};

//// End:: Overlay Helpers

// extends a dictionary object to default values
// a helperfunction
qi__ExtendDict = function()
{
    for (var i = 1; i < arguments.length; i++)
        for (var key in arguments[i])
            if (arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
}

// Returns a div element with specified id, html, class etc
qiGetElement = function(context)
{
    // the default values of expected parameters
	var defaults = {
		font: '20px',
		color: '#000',
		bg: '#fff',
		id: 'undefined',
		classN: 'undefined',
        html: 'undefined',
	};

    context = qi__ExtendDict(defaults, context);
	
	var e = document.createElement('div');
	e.style.top='0px';
	e.style.left='0px';
	e.style.color=context.color;
	e.style.background=context.bg;
	e.style.opacity='1';
	if(context.id != 'undefined') e.id=context.id;
	if(context.classN != 'undefined') e.className = context.classN;
	e.style.fontSize=context.font;
	e.style.padding='1px';
	e.style.border='2px solid #000';
	e.style.zIndex='9000';
	if(context.html == 'undefined') e.innerHTML="qi ...";
	else e.innerHTML = context.html;
	
	return e;
};

// loads an external javascript
qiLoadScript = function(script)
{
	qiL.msg("Loading: " + script, "qiLoadScript");
	var my_script = document.createElement('script');
	my_script.setAttribute('src', script);
	document.head.appendChild(my_script);
	qiL.msg("Done loading: " + script, "qiLoadScript");
};

// if the url starts with the given urlstr
qiUrlStartswith = function(urlstr){
	return (document.URL.lastIndexOf(urlstr, 0) === 0);
};

// if the list contains the item
qiContains = function(list, item){
	return (list.indexOf(item) > -1);
};

// scroll down to the bottom of the page
qiScrollDown = function(){
	window.scrollTo(0, document.body.scrollHeight);
};

// if jQuery not already loaded, load it
qiLoadJquery = function(){
	if (typeof jQuery == 'undefined')
		qiLoadScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js");
};

///////////////////
// QI ENDS HERE //
/////////////////

qi_no = 0;

XDlSpec = function(){
	qi_no = parseInt(prompt("Enter no: ", qi_no + 1), 10);
	if (qi_no != NaN && qi_no <= qiLinks.length)
	{
		qiOverlayImage(qiThumbs[qi_no]);
		if (confirm("Download this image?") === true)
		{
			qiSaveToDisk(qiLinks[qi_no]);
		}
	}
	else alert("Invalid input!");
	qiOverlayHtml("<input type='button' onclick='XDlSpec();' value='Download Specific' style='height:50px; width:350px'/>");
};

Xmain = function(){
		qiLinks = [];
		qiThumbs = [];
		
		var cancelledThrice = 0;
		var downloaded = 0;

    // get all the image links
    var divs = document.getElementsByClassName('thumb');
		for (var i = 0; i < divs.length; i++)
		{
			var link = divs[i].getElementsByTagName("a")[0];
			var thumb = divs[i].getElementsByTagName("img")[0];
			// grab the href
			qiLinks.push(link.href);
			// also the thumbnail
			qiThumbs.push(thumb.src);
            divs[i].appendChild(qiGetElement({id: "overlay-" + i, html: i}));
		}
		
		qiL.msg(qiF("Total {0} images found!", qiLinks.length));
		
		if (confirm("Download all images?") === true){
			for (var i=0; i < qiLinks.length; i++)
			{
				// set the thumbnail on the overlay
				qiOverlayImage(qiThumbs[i]);
				if (confirm("Download this image?") === true)
				{
					qiSaveToDisk(qiLinks[i]);
					cancelledThrice = 0;
					downloaded++;
				}
				else
				{
					// if consequtive 2 cancels, ask if user wants to quit
					if (cancelledThrice > 1){
						if (confirm("Cancel downloading?")) break;
						else cancelledThrice = 0;
					}
					else cancelledThrice++;
				}
			}
			qiL.info(qiF("{0} images downloaded!", downloaded));
		}

		qiOverlayHtml("<input type='button' onclick='XDlSpec();' value='Download Specific' style='height:50px; width:350px'/>");
		
    return;
};

(function(){
try{
	var is_chrome = /chrome/i.test( navigator.userAgent );
	// if on chrome create overlay and place a download button there
	if (is_chrome)
	{
		qiShowOverlay("pfa ...");
		qiOverlayHtml("<input type='button' onclick='Xmain();' value='Download Images' style='height:50px; width:350px'/>");
	}
	else
	{
		if (confirm("Download these images?")) {
			qiShowOverlay("pfa ...");
			Xmain();
		}
	}
	
} catch(e) { alert(e.message); }
})();
