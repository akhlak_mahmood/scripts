// ==UserScript==
// @name           	Pinterest Mobile Downloader
// @namespace    	https://crystaldice.wordpress.com/
// @version         1.0
// @description    	Download pins from a pinterest board while on mobile
// @author          Akhlak Mahmood
// @include         https://www.pinterest.com/*
// @exclude        	https://www.pinterest.com/pin/*
// @run-at          document-end
// @grant           none
// ==/UserScript==

var xkcdMaxScroll = 20;         // collects approx. 20 * ~25  ~500 images

function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var event = document.createEvent('Event');
        event.initEvent('click', true, true);
        save.dispatchEvent(event);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
}

xkcdWaitForAjax = false;
xkcdVisitedLinks = [];
xkcdDloadedImgs = [];
xkcdScrolled = 0;
xkcdGoBackWait = 0;

$(document).ajaxStop(function() {
    // this will be called when all running AJAX calls have completed
    if(xkcdWaitForAjax){
        xkcdWaitForAjax = false;

        setTimeout(function() {

            var images = document.getElementsByClassName('pinImage');
            if(images.length > 0) {
                image = images[0];

                // Image was not downloaded before
                if(xkcdDloadedImgs.indexOf(image.src) < 0) {
                    SaveToDisk(image.src, image.alt);
                    xkcdDloadedImgs.push(image.src);
                    console.log("XKCD >> Pic downloaded!");
                }
                else console.log("XKCD >> Already downloaded!");
            }

        }, 3000);

        setTimeout(function() {
            history.back();
        }, 3000);

        setTimeout(function() {
            // Dispatch/Trigger/Fire the event
            document.dispatchEvent(xkcdEvent);            
        }, 5000);

    } // endif waitForAjax
});

function main() {
    // If its a pin page, return immediately
    if ( document.URL.lastIndexOf("https://www.pinterest.com/pin/", 0) === 0 ) return;
    if ( document.URL.lastIndexOf("http://www.pinterest.com/pin/", 0) === 0 ) return;

    xkcdEvent = new CustomEvent("eventDownloaded", { "detail": "XKCD >> Pin is processed." });
    console.log("XKCD >> Getting all the links ... ");

    // get all the image links
    xkcdLinks = document.getElementsByClassName('pinImageWrapper draggable');
    xkcdIdx = 0;

    console.log("XKCD >> Found " + xkcdLinks.length + " links!");

    if( xkcdLinks.length > 0 )
        document.dispatchEvent(xkcdEvent);

    return;
}

function linkHandler(e) {

    if (xkcdLinks.length < 1) {
        if (xkcdGoBackWait > 15) {
            console.log("XKCD >> Max goBack wait reached!! ");
            return;
        }
        setTimeout(function() {
            xkcdGoBackWait++;
            console.log("XKCD >> Waited for goBack: " + xkcdGoBackWait);
            return linkHandler(null);
        }, 2000);
    }
    else xkcdGoBackWait = 0;

    // if already visited ignore it, select the next one
    while(xkcdVisitedLinks.indexOf(xkcdLinks[xkcdIdx]) > -1) {
        console.log("XKCD >> Already visited: " + xkcdIdx);
        xkcdIdx++;
    }

    if(xkcdIdx < xkcdLinks.length) {
        xkcdLinks[xkcdIdx].click();
        xkcdVisitedLinks.push(xkcdLinks[xkcdIdx]);
        xkcdWaitForAjax = true;
        console.log("XKCD >> Clicked: " + xkcdIdx);
        console.log("XKCD >> Remains: " + (xkcdLinks.length - xkcdIdx));
        xkcdEvent = new CustomEvent("eventDownloaded", { "detail": "XKCD >> Pin is processed." });
        xkcdIdx++;
    }
    else {
        if (xkcdLinks.length < 1) return;
        console.log("XKCD >> All links visited! Scrolling to the bottom ...");

        if ( xkcdScrolled > xkcdMaxScroll )
            console.log("XKCD >> Max scroll reached!");
        else {
            window.scrollTo(0, document.body.scrollHeight);
            xkcdScrolled++;
            setTimeout(function() {
                return main();
            }, 3000);
        }
    }

    return;
}

document.addEventListener("eventDownloaded", linkHandler);

(function() {
    if ( confirm("Download these images?") == false ) return;
    main();
})();
