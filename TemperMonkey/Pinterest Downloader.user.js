// ==UserScript==
// @name            Pinterest Downloader
// @namespace    	http://crystaldice.wordpress.com/
// @version         1.0
// @description     This userscript lets you download the pins of any pinterest board
// @author          Akhlak Mahmood
// @include         https://www.pinterest.com/*
// @exclude         https://www.pinterest.com/pin/*
// @run-at          document-end
// @grant           none
// ==/UserScript==

var xkcdMaxScroll = 20;         // collects approx. 20 * 25  ~500 images

function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var event = document.createEvent('Event');
        event.initEvent('click', true, true);
        save.dispatchEvent(event);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
}

xkcdWaitForAjax = false;
xkcdVisitedLinks = [];
xkcdDloadedImgs = [];
xkcdScrolled = 0;

$(document).ajaxStop(function() {
    // this will be called when all running AJAX calls have completed
    if(xkcdWaitForAjax){
        xkcdWaitForAjax = false;

        setTimeout(function() {
			// get the image
            var images = document.getElementsByClassName('pinImage');
            if(images.length > 0) {
                image = images[0];

                // Image was not downloaded before
                if(xkcdDloadedImgs.indexOf(image.src) < 0) {
                    SaveToDisk(image.src, image.alt);
                    xkcdDloadedImgs.push(image.src);
                    console.log("XKCD >> Image downloaded!");
                }
                else console.log("XKCD >> Already downloaded!");
            }

            // Dispatch/Trigger/Fire the event
            document.dispatchEvent(xkcdEvent);

            // document.getElementsByClassName('Button borderless close visible')[0].click();
        }, 3000);
    }
});

function main() {
    if ( confirm("Download these images?") == false ) {
        // alert("Reload the page, if you change your mind!");
        return;
    }

    xkcdEvent = new CustomEvent("eventDownloaded", { "detail": "XKCD >> Pin is processed." });
    console.log("XKCD >> Getting all the links ... ");
    // get all the image links
    xkcdLinks = document.getElementsByClassName('pinImageWrapper draggable');
    xkcdIdx = 0;
    console.log("XKCD >> Found " + xkcdLinks.length + " links!");
    document.dispatchEvent(xkcdEvent);
}

document.addEventListener("eventDownloaded", function(e) {
    console.log(e.detail);

    // if already visited ignore it, select the next one
    while(xkcdVisitedLinks.indexOf(xkcdLinks[xkcdIdx]) > -1) {
        console.log("XKCD >> Already visited: " + xkcdIdx);
        xkcdIdx++;
    }

    if(xkcdIdx < xkcdLinks.length) {
        xkcdLinks[xkcdIdx].click();
        xkcdVisitedLinks.push(xkcdLinks[xkcdIdx]);
        xkcdWaitForAjax = true;
        console.log("XKCD >> Clicked: " + xkcdIdx);
        console.log("XKCD >> Remains: " + (xkcdLinks.length - xkcdIdx));
        xkcdEvent = new CustomEvent("eventDownloaded", { "detail": "XKCD >> Pin is processed." });
        xkcdIdx++;
    }
    else {
        console.log("XKCD >> All links visited! Scrolling to the bottom ...");
        window.scrollTo(0, document.body.scrollHeight);
        xkcdScrolled++;
        if( xkcdScrolled > xkcdMaxScroll ) console.log("XKCD >> Max scroll reached!");
        else main();
    }
});


window.onload = main;
