Normally I have to write some scripts for task automation or other purposes.
They can be in Python, JavaScript, Bash, Shell etc.
This repository contains all the scripts I make so that they can be versioned and used in future.
